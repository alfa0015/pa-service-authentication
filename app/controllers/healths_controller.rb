
# Clase custom para registro de devise
class HealthsController < ActionController::API
  respond_to :json

  def index
    db_connected = postgres_connected
    db_migration = postgres_migrations_updated
    if db_connected && db_migration
      render json:{connected: db_connected, migrations_updated: db_migration}, status: :ok
    else
      render json:{connected: db_connected, migrations_updated: db_migration}, status: :bad_request

    end
  end

  protected

  def postgres_connected
    ApplicationRecord.connected?
    return true
  rescue StandardError
    false
  end

  def postgres_migrations_updated
    return false unless postgres_connected
    !ApplicationRecord.connection.migration_context.needs_migration?
  end
end