module Api
  module V1
    class UsersController < ApplicationController
      before_action :doorkeeper_authorize!
      def me
        render json: UserSerializer.new(current_resource_owner).serialized_json, status: :ok
      end
    end
  end
end