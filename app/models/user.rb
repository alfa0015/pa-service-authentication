# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, :password_confirmation, presence: true
  validates :email, uniqueness: true
  validates :password, :password_confirmation, length: { minimum: 8 }

  has_many :tokens,
           class_name: 'Doorkeeper::AccessToken',
           foreign_key: :resource_owner_id, inverse_of: false,
           dependent: :destroy
  has_one :token,
          -> { order 'created_at DESC' },
          class_name: 'Doorkeeper::AccessToken',
          foreign_key: :resource_owner_id,
          inverse_of: false,
          dependent: :destroy
end
