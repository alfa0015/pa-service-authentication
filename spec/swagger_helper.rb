require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.join('swagger').to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.json' => {
      openapi: '3.0.1',
      info: {
        title: 'API V1',
        version: 'v1'
      },
      components: {
        securitySchemes: {
          bearerAuth: {
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'JWT'
          }
        },
        schemas: {
          user: {
            properties: {
              email: {
                type: 'string',
                format: 'email',
                required: true
              },
              password: {
                type: 'string',
                format: 'password',
                required: true
              },
              password_confirmation: {
                type: 'string',
                format: 'password',
                required: true
              }
            },
            example: {
              email: 'root@localhost.local',
              password: 'password',
              password_confirmation: 'password'
            }
          },
          token: {
            properties: {
              email: {
                type: 'string',
                format: 'email',
                required: true
              },
              password: {
                type: 'string',
                format: 'password',
                required: true
              },
              grant_type: {
                type: 'string',
                format: 'password',
                required: true,
                enum: %w[password]
              }
            },
            example: {
              email: 'root@localhost.local',
              password: 'password',
              grant_type: 'password'
            }
          },
        }
      },
      paths: {}
    }
  }
end
