# frozen_string_literal: true

# spec/integration/registration_spec.rb
require 'swagger_helper'

describe 'Registration API' do
  path '/api/v1/oauth/token  ' do
    post 'Create a token session' do
      tags 'Sessions'
      description 'Create a new user token'
      consumes 'application/json'
      produces 'application/json'
      request_body_json schema: {
        '$ref' => '#/components/schemas/token'
      }

      response '201', 'user created' do
        schema '$ref' => '#/components/schemas/token'
        run_test!
      end

      response '422', 'invalid request' do
        schema '$ref' => '#/components/schemas/token'
        run_test!
      end
    end
  end
end