# frozen_string_literal: true

# spec/integration/registration_spec.rb
require 'swagger_helper'

describe 'Registration API' do
  path '/api/v1/me' do
    get 'Get user information form user authenticate' do
      tags 'Sessions'
      security [ bearerAuth: [] ]
      description 'Get form current user'
      consumes 'application/json'
      produces 'application/json'

      response 200, 'request sucess' do
        run_test!
      end

      response 401, 'unathorized invalid token' do
        run_test!
      end
    end
  end
end