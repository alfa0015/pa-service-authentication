# frozen_string_literal: true

# spec/integration/registration_spec.rb
require 'swagger_helper'

describe 'Registration API' do
  path '/api/v1/users' do
    post 'Registration a user' do
      tags 'Users'
      description 'Create a new user'
      consumes 'application/json'
      produces 'application/json'
      request_body_json schema: {
        '$ref' => '#/components/schemas/user'
      }

      response '201', 'user created' do
        schema '$ref' => '#/components/schemas/user'
        run_test!
      end

      response '422', 'invalid request' do
        schema '$ref' => '#/components/schemas/user'
        let(:user) { { user: { email: 'foo' } } }
        run_test!
      end
    end
  end
end
