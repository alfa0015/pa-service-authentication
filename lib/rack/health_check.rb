# frozen_string_literal: true

# health check
module Rack
  # "lib/rack/health_check.rb"
  class HealthCheck
    def call(env)
      status = {
        postgres: {
          connected: postgres_connected,
          migrations_updated: postgres_migrations_updated
        }
      }
      if postgres_connected && postgres_migrations_updated
        [200, {}, [status.to_json]]
      else
        [400, {}, [status.to_json]]
      end
    end

    protected

    def postgres_connected
      ApplicationRecord.establish_connection
      ApplicationRecord.connection
      ApplicationRecord.connected?
      ApplicationRecord.remove_connection
      return true
    rescue StandardError
      false
    end

    def postgres_migrations_updated
      return false unless postgres_connected
      ApplicationRecord.establish_connection
      ApplicationRecord.connection
      !ApplicationRecord.connection.migration_context.needs_migration?
      ApplicationRecord.remove_connection
    end
  end
end