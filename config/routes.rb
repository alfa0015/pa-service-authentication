Rails.application.routes.draw do
  use_doorkeeper
  mount OpenApi::Rswag::Ui::Engine => '/api-docs'
  mount OpenApi::Rswag::Api::Engine => '/api-docs'
  get '/health', to: 'healths#index', as: :health_check
  scope :api, defaults: { format: :json } do
    scope :v1 do
      devise_for :users,
                 skip: [:sessions],
                 defaults: {
                   format: :json
                 },
                 controllers: {
                   registrations: 'api/v1/registrations'
                 }
      use_doorkeeper do
        controllers tokens: 'api/v1/access_token'
        skip_controllers :applications, :authorized_applications, :authorizations
      end
    end
  end
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      get '/me', to: 'users#me', as: :me
    end
  end
end
