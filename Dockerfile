FROM ruby:2.6-alpine
RUN apk --no-cache --update add build-base bash git nodejs tzdata postgresql-dev postgresql-client libxslt-dev libxml2-dev imagemagick sqlite sqlite-dev busybox-extras shadow busybox-suid coreutils
RUN bundle config git.allow_insecure true
WORKDIR /app
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN adduser -D -u 1001 rails && usermod -aG 0 rails
RUN chown -R 1001 /app && chgrp -R 0 /app && chmod -R g=u /app
RUN chmod +x /usr/bin/entrypoint.sh
USER 1001
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]